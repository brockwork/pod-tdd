const noSound = message => message.replace(/\s+/g, '') === '';

export const hey = (message) => {
  if (noSound(message)) {
    return 'Fine. Be that way!';
  }
 
  return 'Whatever.';
};