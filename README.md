# TDD Bob
Inspired by the 'Deaf Grandma' exercise in Chris Pine's Learn to Program tutorial. http://pine.fm/LearnToProgram/?Chapter=06

## Summary
Change xtest to test within bob.spec.js after writing passing code in bob.js.

## Installing

```bash
$ npm install
```
## Running Tests

```bash
$ npm test
```