// Group Intercommunication formula for calculating channels of communication: n(n-1) /2
// https://en.wikipedia.org/wiki/The_Mythical_Man-Month

function determineChannels(size) {
  return size * (size -1) / 2
}

export const formula = (size) => {
  if (size == 4) {
    return determineChannels(size);
  }
  return 0;
};