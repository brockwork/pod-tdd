import { formula } from './interCommunicationFormula';

describe('Intercommunication Formula', () => {
  test('small team', () => {
    const numberOfCommunicationChannels = formula(4);
    expect(numberOfCommunicationChannels).toEqual(6);
  });
  test('medium team', () => {
    const numberOfCommunicationChannels = formula(10);
    expect(numberOfCommunicationChannels).toEqual(45);
  });
  xtest('big team', () => {
    const numberOfCommunicationChannels = formula(50);
    expect(numberOfCommunicationChannels).toEqual(1225);
  });
  xtest('really big team', () => {
    const numberOfCommunicationChannels = formula(1000);
    expect(numberOfCommunicationChannels).toEqual(499500);
  });
});